<?php
/**
 * @file
 * Provide all the administration pages
 */

/**
 * Implementation of hook_settings().
 */
function statistics_ajax_settings() {
  $form = array();
  $form['statistics_ajax_limit_requests_xmlhttprequest'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow ONLY xmlhttprequest method (AJAX)'),
    '#default_value' => variable_get('statistics_ajax_limit_requests_xmlhttprequest', 1),
    '#description' => t('Restricts the node counter statistics update to
       AJAX xmlhttprequests (through javascript)'),
  );
  $form['statistics_ajax_limit_requests_get'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow HTTP GET method'),
    '#default_value' => variable_get('statistics_ajax_limit_requests_get', 1),
    '#description' => t('Allows HTTP GET requests to update the node counter
      statistics'),
  );
  $form['statistics_ajax_limit_requests_post'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow HTTP POST method'),
    '#default_value' => variable_get('statistics_ajax_limit_requests_post', 1),
    '#description' => t('Allows HTTP POST requests to update the node counter
      statistics'),
  );

  return node_types($form) + system_settings_form($form);
}

/**
 * Añade elementos a un formulario para seleccionar en qué tipos de contenidos aplica.
 *
 * @param $form
 *
 * @return array
 *   $form
 */
function node_types($form) {
  $node_types = node_get_types();
  foreach ($node_types as $key => $val) {
    $node_types[$key] = $val->name;
  }

  $statistics_ajax_node_types_set = variable_get('statistics_ajax_node_types_set', 'all');
  $form['node_types'] = array(
  	'#type' => 'fieldset',
    '#title' => t('Only update specific node types'),
    '#collapsible' => TRUE,
    '#collapsed' => $statistics_ajax_node_types_set,
  );

  $form['node_types']['statistics_ajax_node_types_set'] = array(
    '#type' => 'radios',
    '#title' => t('Perform action for'),
    '#options' => array(
    	'all' => t('all node types'),
      'selected' => t('selected node types (below)'),
    ),
  	'#default_value' => $statistics_ajax_node_types_set,
    '#required' => TRUE,
    '#attributes' => array('onclick' => '
    	if ($(this).attr(\'id\') == \'edit-statistics-ajax-node-types-set-all\') {
      	$(\'#node_types_selected\').css("display", "none");
      	$(\'#node_types_selected input\').attr(\'checked\', false);
      } else {
      	$(\'#node_types_selected\').css("display", "block");
			}
    '),
  );

  if ($statistics_ajax_node_types_set == 'all') {
    $style = 'style="display:none"';
  }
  $form['node_types']['statistics_ajax_node_types_selected'] = array(
  	'#prefix' => '<div id="node_types_selected" ' . $style . '>',
    '#suffix' => '</div>',
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#default_value' => variable_get('statistics_ajax_node_types_selected', ''),
    '#options' => $node_types,
    '#description' => t('Select the node types this action should affect'),
  );

  return $form;
}
