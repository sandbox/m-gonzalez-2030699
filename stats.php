<?php
/**
 * @file
 *
 * @author martin_gonzalez
 *
 * set it at webroot!
 */

// This verification we leave for safety, but varnish add a rule to not take a heavy 404.
if (!isset($_GET['nid'])) {
  statsLog("---Missing NID, surely someone is trying something---");
  statistics_stats_full_boot();
  drupal_not_found();
  exit;
}

// connect to db & set variables.
statistics_stats_init();

global $nid;
$statistics_ajax_node_types_set = statistics_variable_get('statistics_ajax_node_types_set', 'all');
$type = db_result(db_query("SELECT type FROM {node} WHERE nid = %d", $nid));
if ($statistics_ajax_node_types_set == 'all' || in_array($type, statistics_variable_get('statistics_ajax_node_types_selected', ''))) {
  statistics_ajax_update_counter();
}
else {
  statsLog("Can't update the counter of this node: " . $nid);
}
// end of script, exit.

exit;


/**
 * Set the minimum required to run the script.
 */
function statistics_stats_init() {
  global $nid, $men;

  // Connect to DB.
  include_once './includes/bootstrap.inc';
  include_once './includes/common.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

  // Init memcache.
  include_once './sites/all/modules/contrib/memcache/memcache.inc';
  $men = dmemcache_object();

  // Set variables passed via GET.
  $nid = (isset($_GET['nid']) && is_numeric($_GET['nid'])) ? $_GET['nid'] : NULL;
}

/**
 * Validates we are updating with ajax request and nid to be valid. After memcache updated node access statistics.
 *
 * @return JSON indicating success or failure
 */
function statistics_ajax_update_counter() {
  // check if restricted to AJAX only
  if (statistics_variable_get('statistics_ajax_limit_requests_xmlhttprequest', 0)) {
    // check server headers
    if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
      && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
      statsLog("---AJAX must be used---");
      drupal_json(array(
        'status' => t('error'),
        'data' => t('AJAX must be used'),
      ));
      exit;
    }
  }
  // check if HTTP POST is allowed
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // check if this method is allowed
    if (!statistics_variable_get('statistics_ajax_limit_requests_post', 0)) {
      statsLog("---POST is not allowed---");
      drupal_json(array(
        'status' => t('error'),
        'data' => t('POST is not allowed'),
      ));
      exit;
    }
  }
  // check if HTTP GET is allowed
  elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // check if this method is allowed
    if (!statistics_variable_get('statistics_ajax_limit_requests_get', 0)) {
      statsLog("---GET is not allowed---");
      drupal_json(array(
        'status' => t('error'),
        'data' => t('GET is not allowed'),
      ));
      exit;
    }
  }
  // Ensure the HTTP method is either GET or POST - other methods
  // are denied (e.g. HEAD, OPTIONS)
  else {
    statsLog("---unknown HTTP method---");
    drupal_json(array(
      'status' => t('error'),
      'data' => t('unknown HTTP method'),
    ));
    exit;
  }

  global $men;

  if ($men !== FALSE) {
    incrementCount();
  }
  else {
    statsLog("An error initializing memcache");
  }
}

/**
 * Counter increment.
 */
function incrementCount() {
  global $nid, $men;

  // prepare post key
  $key = "post_id_" . $nid . "_count";

  $new_count = $men->increment($key, 1);
  if($new_count === FALSE) {
    $exp = 86400; // 1 día.
    if ($men instanceof Memcached) {
      $new_count = $men->add($key, 1, $exp);
    }
    elseif ($men instanceof Memcache) {
      $new_count = $men->add($key, 1, 0, $exp);
    }
    if($new_count === FALSE) {
      statsLog("Someone raced us for first count on key " . $key);
      $new_count = $men->increment($key, 1);
      if($new_count === FALSE) {
        statsLog("Unable to increment key " . $key);
        return FALSE;
      }
      else {
        statsLog("Incremented key " . $key . " to " . $new_count);
        return TRUE;
      }
    }
    else {
      statsLog("Initialized key " . $key . " to " . $new_count);
      return TRUE;
    }
  }
  else {
    statsLog("Incremented key " . $key . " to " . $new_count);
		return TRUE;
  }
}

/**
 * Init DRUPAL_BOOTSTRAP_FULL
 */
function statistics_stats_full_boot() {
  Global $full_boot;
  if (!isset($full_boot)) {
    $full_boot = FALSE;
  }
  if (!$full_boot) {
    include_once './includes/bootstrap.inc';
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  }
  $full_boot = TRUE;
}

/**
 * Returns the value of a variable stored in memcached from its name.
 *
 * @param string $name
 *   Variable name.
 *
 * @return value
 *   Returns the value of the variable if it exists, false otherwise.
 */
function statistics_variable_get($name, $default) {
  static $instance;
  if (!isset($instance)) {
    $cache_item = cache_get('variables', 'cache');
    $instance = $cache_item->data;
  }
  return isset($instance[$name]) ? $instance[$name] : $default;
}

/**
 * Generate log.
 *
 * @param string $msg
 *   Message Log.
 */
function statsLog($msg) {
  if (isset($_SERVER['HTTP_USER_AGENT'])) {
    $msg .= ' , HTTP_USER_AGENT: ' . $_SERVER['HTTP_USER_AGENT'];
  }
  error_log($msg);
  /*
  // It may be helpful especially in times of development, show the logs in a file.
  if ($log_to_file) {
    $file = 'stats_log.txt';
    // Write the contents to the file,
    // using the FILE_APPEND flag to append the content to the end of the file
    // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
    file_put_contents($file, $msg . "\n", FILE_APPEND | LOCK_EX);
  }
  */
}